import * as models from '../model/model'
import jwt from 'jsonwebtoken'

export const Products_male = function (req, res) {
  models.Products.find({ sex: "male" }, function (eqq, result) {
    if (result == null) res.json({ data: [] });
    else {
      res.json(result);
    }
  });
};

export const Products_female = function (req, res) {
  models.Products.find({ sex: "female" }, function (eqq, result) {
    if (result == null) res.json({ data: [] });
    else {
      res.json(result);
    }
  });
};

export const Products = function (req, res) {
  models.Products.find({}, function (eqq, result) {
    if (result == null) res.json({ data: [] });
    else {
      res.json(result);
    }
  });
};

export const Product_id = function (req, res) {
  var { id } = req.params;
  models.Products.find({ id: id }, function (eqq, result) {
    if (result == null) res.json({ data: [] });
    else {
      res.json(result);
    }
  });
};

export const User = function (req, res) {
  var { mail, password } = req.body;
  models.Users.find({ mail: mail, password: password }, function (err, result) {
    if (result[0] == null) {
      res.send(false);
    } else {
      jwt.sign(mail, "token", function (err, token) {
        res.send(token);
      });
    }
  });
};

export const Token = function (req, res) {
  var { token } = req.body;
  jwt.verify(token, "token", function (err, value) {
    models.Users.find({ mail: value }, function (err, result) {
      if (result[0] == null) {
        res.send(false);
      } else {
        res.send(true);
      }
    });
  });
};

export const Registration = function (req, res) {
  var { mail, password } = req.body;
  if (mail != "") {
    models.Users.find({ mail: mail }, function (err, result) {
      if (result[0] == null) {
        models.Users.create({ mail, password }, function (err, resault) {
          if (err) {
            throw err;
          }
        });
        res.send(true);
      } else {
        res.send(false);
      }
    });
  } else {
    res.send("");
  }
};

export const Filter = function (req, res) {
  var { id } = req.params;
  console.log(id)
  models.Products.find({}, function (eqq, result) {
    if (result == null) res.json({ data: [] });
    else {
      result = result.filter((product) => {
        return (
          product.name.toLowerCase().replace(/\s/g, "").indexOf(id.toLowerCase().replace(/\s/g, "")) !== -1
        );
      });
      res.send(result);
    }
  });
};
