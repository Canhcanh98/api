var mongoose = require("mongoose");

const url =
  "mongodb+srv://canh:canh0905629846@cluster0-pyckq.mongodb.net/test?retryWrites=true&w=majority";
  

mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
const connection = mongoose.connection;

connection
  .on("error", (err) => {
    console.log("Error: ", err);
  })

  .once("open", () => {
    console.log("mongoDB database connection established");
  });
var Schema = mongoose.Schema;

var products = new Schema({
  id: String,
  name: String,
  img: String,
  img2: String,
  img3: String,
  img4: String,
  normalPrice: Number,
  salePrice: Number,
  description: Array,
  inventory: Number,
  rating: Number,
  sex: String,
  discount: Number,
  size: Number,
});
export const Products = mongoose.model("products", products);

var user = new Schema({
  mail: String,
  password: String,
});

export const Users = mongoose.model("uers", user);
