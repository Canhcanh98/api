import {
  Products_male,
  Products_female,
  Products,
  Product_id,
  User,
  Filter,
  Registration,
  Token,
} from "../controllers/controller";

const routes = function (app, Passport, jwt) {
  app.get("/products/male", Products_male);

  app.get("/products/female", Products_female);

  app.get("/products", Products);

  app.get("/products/:id", Product_id);

  app.get("/filter/:id", Filter);

  app.post("/users", User);

  app.post("/registration", Registration);

  app.post("/token", Token);
};

export default routes;
